FROM webdevops/php-nginx:8.0-alpine

RUN apk add --update runuser

RUN docker-php-ext-install pdo pdo_mysql mysqli

RUN mkdir -p /var/www/html

COPY ./scripts/composer-setup.sh /opt/docker/provision/entrypoint.d/

WORKDIR /var/www/html
