#!/bin/bash

if [[ $DEV -eq 1 ]]; then
  echo "DEVELOPER MODE Enabled..."
  RETRY_CNT=10
  STATUS=-1
  # Install composer libraries
  until [[ $RETRY_CNT -le 0  || $STATUS -eq 0 ]]; do
    if [ -f /var/www/html/composer.json ]; then
        echo "DEVMODE: Installing PHP Libraries with Composer"
        runuser application -c 'composer install --no-interaction --no-plugins'
        STATUS=$?
    fi
    sleep 1
    ((RETRY_CNT--))
  done
  if [ $STATUS -ne 0 ]; then
    echo "DEVMODE: Failed to Install Composer Libraries: $STATUS"
    exit $STATUS
  fi
  echo "DEVMODE: Composer Libraries Installed"
fi

#
# Perform Migration
#
echo "Performing Migration Test"
runuser application -c 'php artisan migrate --force'
STATUS=$?
echo "Returned $STATUS"
if [ $STATUS -ne 0 ]; then
  echo "Failed to perform Database Migration: $STATUS"
  exit $STATUS
fi
