# Profiling System v2

1. git clone git@gitlab.lanexus.com:lanex-internal/profiling-system.git profiling-system
2. cd profiling-system
3. cp api/.env.example api/.env
4. docker-compose up -d 
5. go to http://localhost:8888


### To execute composer and artisan commands
1. docker-compose exec --user application api bash
2. ..then execute your composer / artisan commands

### Automation
1. Unmigrated migration files are automatically migrated when running the api container